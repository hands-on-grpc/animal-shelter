package com.git.hugovallada.extensions

import com.git.hugovallada.AnimalResponseGrpc
import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.model.Animal

fun AnimalDomain.translateToModel() = Animal(
        publicId = publicId ?: Animal.generateUUID(), breed = this.breed,
        age = this.age, temperament = this.temperament,
        availableForAdoption = this.availableForAdoption, previousAdopted = this.previousAdopted,
        idTag = this.idTag, id = id
)

fun AnimalDomain.translateToGrpc() = AnimalResponseGrpc.newBuilder()
        .setBreed(breed).setAge(age).setId(publicId.toString())
        .setTemperament(temperament).setAvailableForAdoption(availableForAdoption)
        .setPreviousAdopted(previousAdopted).setIdTag(idTag.toString()).build()