package com.git.hugovallada.extensions

import com.git.hugovallada.AnimalIdRequestGrpc
import com.git.hugovallada.AnimalIdRequestGrpc.FilterCase.IDTAG
import com.git.hugovallada.AnimalIdRequestGrpc.FilterCase.PUBLICID
import com.git.hugovallada.AnimalRequestGrpc
import com.git.hugovallada.AnimalUpdateRequestGrpc
import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.exceptions.BadRequestException
import com.git.hugovallada.utils.Filter
import java.util.*

fun AnimalRequestGrpc.translateToDomain() = AnimalDomain(
        breed = this.breed, age = this.age, temperament = this.temperament,
        availableForAdoption = this.availableForAdoption, previousAdopted = this.previousAdopted,
        idTag = UUID.fromString(this.idTag)
)

fun AnimalIdRequestGrpc.translateToFilter(): Filter {
    val filter = when (filterCase) {
        PUBLICID -> publicId.let {
            Filter.WithPublicId(UUID.fromString(it))
        }

        IDTAG -> idTag.let {
            Filter.WithIdTag(UUID.fromString(it))
        }

        else -> throw BadRequestException("Invalid Filter")

    }

    return filter
}

fun AnimalUpdateRequestGrpc.toDomain() = AnimalDomain(
        breed = "", age = age, temperament = "",
        availableForAdoption = availableForAdoption, previousAdopted = previousAdopted,
        idTag = UUID.fromString(idTag)
)