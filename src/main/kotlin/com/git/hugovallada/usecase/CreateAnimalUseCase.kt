package com.git.hugovallada.usecase

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.exceptions.AlreadyReportedException
import com.git.hugovallada.gateway.CreateAnimalGateway
import com.git.hugovallada.gateway.ExistsAnimalByIdTagGateway
import io.micronaut.validation.Validated
import jakarta.inject.Inject
import jakarta.inject.Singleton
import javax.validation.Valid

@Validated
@Singleton
class CreateAnimalUseCase(@Inject private val createAnimalGateway: CreateAnimalGateway,
                          private val existsAnimalByIdTagGateway: ExistsAnimalByIdTagGateway) {

    fun execute(@Valid animalDomain: AnimalDomain): AnimalDomain {
        if (existsAnimalByIdTagGateway.execute(animalDomain))
            throw AlreadyReportedException("There's already an animal with the tag: ${animalDomain.idTag}")

        return createAnimalGateway.execute(animalDomain)
    }

}