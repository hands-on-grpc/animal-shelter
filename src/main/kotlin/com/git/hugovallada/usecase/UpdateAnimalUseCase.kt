package com.git.hugovallada.usecase

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.exceptions.NotFoundException
import com.git.hugovallada.gateway.FilterByIdTagGateway
import com.git.hugovallada.gateway.UpdateAnimalGateway
import jakarta.inject.Singleton
import javax.transaction.TransactionScoped

@Singleton
class UpdateAnimalUseCase(private val getAnimalByIdTagGateway: FilterByIdTagGateway, private val updateAnimalGateway: UpdateAnimalGateway) {

    @TransactionScoped
    fun execute(animalDomain: AnimalDomain) {

        getAnimalByIdTagGateway.execute(animalDomain.idTag)?.run {
            val animalUpdated = copy(age = animalDomain.age, availableForAdoption = animalDomain.availableForAdoption, previousAdopted = animalDomain.previousAdopted)
            updateAnimalGateway.execute(animalUpdated)
        } ?: throw NotFoundException("Id tag not found")

    }

}