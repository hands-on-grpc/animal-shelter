package com.git.hugovallada.usecase

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.gateway.GetAllAnimalsGateway
import jakarta.inject.Singleton

@Singleton
class GetAllAnimalsUseCase(private val getAllAnimalsGateway: GetAllAnimalsGateway) {

    fun execute(): List<AnimalDomain> {
        return getAllAnimalsGateway.execute()
    }

}