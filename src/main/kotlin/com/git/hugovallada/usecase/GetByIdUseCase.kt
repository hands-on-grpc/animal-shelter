package com.git.hugovallada.usecase

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.exceptions.NotFoundException
import com.git.hugovallada.gateway.FilterByIdTagGateway
import com.git.hugovallada.gateway.FilterByPublicIdGateway
import com.git.hugovallada.utils.Filter
import jakarta.inject.Singleton

@Singleton
class GetByIdUseCase(private val getByIdTagGateway: FilterByIdTagGateway, private val getByPublicIdGateway: FilterByPublicIdGateway) {

    fun execute(filter: Filter): AnimalDomain {
        return when (filter) {
            is Filter.WithIdTag -> filter.getAnimal(getByIdTagGateway) ?: throw NotFoundException("Animal not found")
            is Filter.WithPublicId -> filter.getAnimal(getByPublicIdGateway)
                    ?: throw NotFoundException("Animal not found")
        }
    }

}