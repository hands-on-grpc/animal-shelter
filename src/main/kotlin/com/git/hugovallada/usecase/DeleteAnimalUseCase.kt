package com.git.hugovallada.usecase

import com.git.hugovallada.gateway.DeleteAnimalByTagGateway
import jakarta.inject.Singleton
import java.util.*

@Singleton
class DeleteAnimalUseCase(private val deleteAnimalByTagGateway: DeleteAnimalByTagGateway) {

    fun execute(idTag: UUID) {
        deleteAnimalByTagGateway.execute(idTag)
    }

}