package com.git.hugovallada.endpoint

import com.git.hugovallada.AnimalIdRequestGrpc
import com.git.hugovallada.AnimalResponseGrpc
import com.git.hugovallada.AnimalsResponseListGrpc
import com.git.hugovallada.GetAnimalsServiceGrpc
import com.git.hugovallada.extensions.translateToFilter
import com.git.hugovallada.extensions.translateToGrpc
import com.git.hugovallada.usecase.GetAllAnimalsUseCase
import com.git.hugovallada.usecase.GetByIdUseCase
import com.git.hugovallada.utils.GrpcIntercept
import com.google.protobuf.Empty
import io.grpc.stub.StreamObserver
import jakarta.inject.Singleton

@Singleton
@GrpcIntercept
class GetAnimalsEndpoint(private val getAllAnimalsUseCase: GetAllAnimalsUseCase, private val getAnimalByIdUseCase: GetByIdUseCase) : GetAnimalsServiceGrpc.GetAnimalsServiceImplBase() {

    override fun findAll(request: Empty, responseObserver: StreamObserver<AnimalsResponseListGrpc>) {

        getAllAnimalsUseCase.execute().run {
            val response = AnimalsResponseListGrpc.newBuilder().addAllAnimals(map { it.translateToGrpc() }).build()
            responseObserver.onNext(response)
            responseObserver.onCompleted()


        }
    }

    override fun findById(request: AnimalIdRequestGrpc, responseObserver: StreamObserver<AnimalResponseGrpc>) {

        val filter = request.translateToFilter()

        getAnimalByIdUseCase.execute(filter).run {
            responseObserver.onNext(translateToGrpc())
            responseObserver.onCompleted()
        }


    }

}