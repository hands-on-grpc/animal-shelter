package com.git.hugovallada.endpoint

import com.git.hugovallada.AnimalRequestGrpc
import com.git.hugovallada.AnimalResponseGrpc
import com.git.hugovallada.NewAnimalServiceGrpc
import com.git.hugovallada.extensions.translateToDomain
import com.git.hugovallada.extensions.translateToGrpc
import com.git.hugovallada.usecase.CreateAnimalUseCase
import com.git.hugovallada.utils.GrpcIntercept
import io.grpc.stub.StreamObserver
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Singleton
@GrpcIntercept
class NewAnimalGrpcEndpoint(@Inject private val createAnimalUseCase: CreateAnimalUseCase) : NewAnimalServiceGrpc.NewAnimalServiceImplBase() {

    override fun create(request: AnimalRequestGrpc, responseObserver: StreamObserver<AnimalResponseGrpc>) {

        createAnimalUseCase.execute(request.translateToDomain()).run {
            responseObserver.onNext(translateToGrpc())
        }
        responseObserver.onCompleted()
    }

}