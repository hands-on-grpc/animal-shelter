package com.git.hugovallada.endpoint

import com.git.hugovallada.AnimalUpdateRequestGrpc
import com.git.hugovallada.SuccessMessageResponseGrpc
import com.git.hugovallada.UpdateAnimalServiceGrpc
import com.git.hugovallada.extensions.toDomain
import com.git.hugovallada.usecase.UpdateAnimalUseCase
import com.git.hugovallada.utils.GrpcIntercept
import io.grpc.stub.StreamObserver
import jakarta.inject.Singleton

@Singleton
@GrpcIntercept
class UpdateAnimalEndpoint(private val updateAnimalUseCase: UpdateAnimalUseCase) : UpdateAnimalServiceGrpc.UpdateAnimalServiceImplBase() {

    override fun update(request: AnimalUpdateRequestGrpc, responseObserver: StreamObserver<SuccessMessageResponseGrpc>) {
        updateAnimalUseCase.execute(request.toDomain()).also {
            responseObserver.onNext(SuccessMessageResponseGrpc.newBuilder().setMessage("Updated").build())
            responseObserver.onCompleted()
        }
    }

}