package com.git.hugovallada.endpoint

import com.git.hugovallada.AnimalDeleteIdTageRequestGrpc
import com.git.hugovallada.DeleteAnimalServiceGrpc
import com.git.hugovallada.SuccessMessageResponseGrpc
import com.git.hugovallada.usecase.DeleteAnimalUseCase
import com.git.hugovallada.utils.GrpcIntercept
import io.grpc.stub.StreamObserver
import jakarta.inject.Singleton
import java.util.*

@Singleton
@GrpcIntercept
class DeleteAnimalEndpoint(private val deleteAnimalUseCase: DeleteAnimalUseCase) : DeleteAnimalServiceGrpc.DeleteAnimalServiceImplBase() {

    override fun deleteByTag(request: AnimalDeleteIdTageRequestGrpc, responseObserver: StreamObserver<SuccessMessageResponseGrpc>) {
        deleteAnimalUseCase.execute(UUID.fromString(request.idTag))

        responseObserver.onNext(SuccessMessageResponseGrpc.newBuilder().setMessage("Deleted").build())
        responseObserver.onCompleted()
    }

}