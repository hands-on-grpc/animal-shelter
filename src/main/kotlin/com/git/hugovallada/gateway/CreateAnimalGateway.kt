package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.extensions.translateToModel
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Singleton
class CreateAnimalGateway(@Inject private val animalRepository: AnimalRepository) {

    fun execute(animalDomain: AnimalDomain): AnimalDomain {
        animalRepository.save(animalDomain.translateToModel()).run {
            return AnimalDomain.of(this)
        }
    }

}