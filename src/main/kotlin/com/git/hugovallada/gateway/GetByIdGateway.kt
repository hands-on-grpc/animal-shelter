package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import java.util.*

interface GetByIdGateway {

    fun execute(id: UUID): AnimalDomain?

}