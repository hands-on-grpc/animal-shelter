package com.git.hugovallada.gateway

import com.git.hugovallada.exceptions.NotFoundException
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Singleton
import java.util.*

@Singleton
class DeleteAnimalByTagGateway(private val animalRepository: AnimalRepository) {

    fun execute(idTag: UUID) {

        if (!animalRepository.existsByIdTag(idTag)) throw NotFoundException("Animal not found")

        animalRepository.findByIdTag(idTag)?.run {
            animalRepository.delete(this)
        }

    }

}