package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.extensions.translateToModel
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Singleton

@Singleton
class UpdateAnimalGateway(private val repository: AnimalRepository) {

    fun execute(animalDomain: AnimalDomain) {
        repository.update(animalDomain.translateToModel())
    }

}