package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Singleton
import java.util.*

@Singleton
class FilterByIdTagGateway(private val repository: AnimalRepository) : GetByIdGateway {

    override fun execute(id: UUID): AnimalDomain? {
        return repository.findByIdTag(id)?.let {
            AnimalDomain.of(it)
        }
    }

}