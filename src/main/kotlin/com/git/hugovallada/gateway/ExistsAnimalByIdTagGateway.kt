package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Singleton

@Singleton
class ExistsAnimalByIdTagGateway(private val animalRepository: AnimalRepository) {

    fun execute(animalDomain: AnimalDomain): Boolean {
        return animalRepository.existsByIdTag(animalDomain.idTag)
    }

}