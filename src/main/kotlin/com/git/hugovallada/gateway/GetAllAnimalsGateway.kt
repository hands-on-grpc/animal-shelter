package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.repository.AnimalRepository
import jakarta.inject.Singleton

@Singleton
class GetAllAnimalsGateway(private val animalRepository: AnimalRepository) {

    fun execute(): List<AnimalDomain> {

        return animalRepository.findAll().map {
            AnimalDomain.of(it)
        }

    }

}