package com.git.hugovallada.domain

import com.git.hugovallada.model.Animal
import io.micronaut.core.annotation.Introspected
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

@Introspected
data class AnimalDomain(
        val id: Long? = null,

        val publicId: UUID? = null,

        @field:NotBlank
        val breed: String,

        @field:NotNull
        @field:Positive
        val age: Int,

        @field:NotBlank
        val temperament: String,

        @field:NotNull
        val availableForAdoption: Boolean,

        @field:NotNull
        val previousAdopted: Boolean,

        @field:NotNull
        val idTag: UUID
) {

    companion object {
        fun of(animal: Animal) = AnimalDomain(
                animal.id, animal.publicId, animal.breed, animal.age,
                animal.temperament, animal.availableForAdoption, animal.previousAdopted, animal.idTag
        )
    }

}