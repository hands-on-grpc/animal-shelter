package com.git.hugovallada.exceptions

class NotFoundException(msg: String) : RuntimeException(msg)