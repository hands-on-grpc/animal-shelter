package com.git.hugovallada.exceptions

class AlreadyReportedException(msg: String) : RuntimeException(msg)