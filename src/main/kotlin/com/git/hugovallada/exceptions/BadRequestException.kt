package com.git.hugovallada.exceptions

class BadRequestException(msg: String) : RuntimeException(msg)