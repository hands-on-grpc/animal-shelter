package com.git.hugovallada.repository

import com.git.hugovallada.model.Animal
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository
import java.util.*

@Repository
interface AnimalRepository : JpaRepository<Animal, Long> {

    fun existsByIdTag(id: UUID): Boolean

    fun findByPublicId(publicId: UUID): Animal?

    fun findByIdTag(idTag: UUID): Animal?
}