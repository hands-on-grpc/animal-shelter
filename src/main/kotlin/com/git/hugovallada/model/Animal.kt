package com.git.hugovallada.model

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "tb_animal")
class Animal(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val publicId: UUID,

        val breed: String,

        val age: Int,

        val temperament: String,

        val availableForAdoption: Boolean,

        val previousAdopted: Boolean,

        val idTag: UUID
) {
    companion object {
        fun generateUUID(): UUID = UUID.randomUUID()
    }
}