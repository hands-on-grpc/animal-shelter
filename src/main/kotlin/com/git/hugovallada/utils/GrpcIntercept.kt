package com.git.hugovallada.utils

import io.micronaut.aop.Around

@MustBeDocumented
@Retention
@Target(AnnotationTarget.CLASS)
@Around
annotation class GrpcIntercept()
