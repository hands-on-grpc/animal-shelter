package com.git.hugovallada.utils

import com.git.hugovallada.exceptions.AlreadyReportedException
import com.git.hugovallada.exceptions.BadRequestException
import com.git.hugovallada.exceptions.NotFoundException
import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.grpc.stub.StreamObserver
import io.micronaut.aop.InterceptorBean
import io.micronaut.aop.MethodInterceptor
import io.micronaut.aop.MethodInvocationContext
import jakarta.inject.Singleton
import javax.validation.ConstraintViolationException

@Singleton
@InterceptorBean(GrpcIntercept::class)
class GrpcInterceptor : MethodInterceptor<Any, Any> {

    override fun intercept(context: MethodInvocationContext<Any, Any>): Any? {
        return try {
            context.proceed()
        } catch (exception: Exception) {
            val response = context.parameterValues[1] as StreamObserver<*>

            when (exception) {
                is BadRequestException -> Status.INVALID_ARGUMENT.withCause(exception)
                        .withDescription(exception.message)

                is NotFoundException -> Status.NOT_FOUND.withCause(exception)
                        .withDescription(exception.message)

                is AlreadyReportedException -> Status.ALREADY_EXISTS.withCause(exception)
                        .withDescription(exception.message)

                is ConstraintViolationException -> Status.INVALID_ARGUMENT.withCause(exception)
                        .withDescription(exception.message)

                else -> Status.UNKNOWN.withCause(exception).withDescription("Unknown Error...")
            }.run {
                response.onError(StatusRuntimeException(this))
            }
        }
    }
}