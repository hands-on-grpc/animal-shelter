package com.git.hugovallada.utils

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.gateway.GetByIdGateway
import io.micronaut.core.annotation.Introspected
import java.util.*
import javax.validation.constraints.NotBlank

@Introspected
sealed class Filter() {

    abstract fun getAnimal(getByIdGateway: GetByIdGateway): AnimalDomain?

    data class WithPublicId(
            @field:NotBlank val publicId: UUID
    ) : Filter() {

        override fun getAnimal(getByIdGateway: GetByIdGateway): AnimalDomain? {
            return getByIdGateway.execute(publicId)
        }

    }

    data class WithIdTag(
            @field:NotBlank val idTag: UUID
    ) : Filter() {
        override fun getAnimal(getByIdGateway: GetByIdGateway): AnimalDomain? {
            return getByIdGateway.execute(idTag)
        }
    }


}
