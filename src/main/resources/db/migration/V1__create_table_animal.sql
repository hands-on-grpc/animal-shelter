create table tb_animal(
    id serial primary key,
    public_id uuid not null,
    breed varchar(50) not null,
    age int not null,
    temperament varchar(30) not null,
    available_for_adoption boolean not null,
    previous_adopted boolean not null
);