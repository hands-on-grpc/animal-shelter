package com.git.hugovallada.endpoint

import com.git.hugovallada.AnimalRequestGrpc
import com.git.hugovallada.AnimalResponseGrpc
import com.git.hugovallada.NewAnimalServiceGrpc
import com.git.hugovallada.extensions.translateToDomain
import com.git.hugovallada.extensions.translateToGrpc
import com.git.hugovallada.repository.AnimalRepository
import com.git.hugovallada.usecase.CreateAnimalUseCase
import io.grpc.ManagedChannel
import io.grpc.StatusRuntimeException
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.equality.shouldBeEqualToIgnoringFields
import io.kotest.matchers.string.shouldContain
import io.micronaut.context.annotation.Factory
import io.micronaut.grpc.annotation.GrpcChannel
import io.micronaut.grpc.server.GrpcServerChannel
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import io.mockk.clearAllMocks
import jakarta.inject.Inject
import jakarta.inject.Singleton
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*

@MicronautTest(transactional = false)
class NewAnimalGrpcEndpointTest {

    @Inject
    private lateinit var animalRepository: AnimalRepository

    @Inject
    private lateinit var createAnimalUseCase: CreateAnimalUseCase

    @Inject
    private lateinit var grpcClient: NewAnimalServiceGrpc.NewAnimalServiceBlockingStub


    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `should return an animal`() {
        val idTag = UUID.randomUUID().toString()
        val request = AnimalRequestGrpc.newBuilder()
                .setAge(10).setBreed("Maltes").setIdTag(idTag)
                .setTemperament("Docil")
                .setAvailableForAdoption(true).setPreviousAdopted(true)
                .build()

        val animalDomain = request.translateToDomain()

        val response = grpcClient.create(request)

        response.shouldBeEqualToIgnoringFields(animalDomain.translateToGrpc(), property = AnimalResponseGrpc::ID_FIELD_NUMBER)

        animalRepository.existsByIdTag(UUID.fromString(idTag)).shouldBeTrue()

    }

    @Test
    fun `should throw a invalid argument exception`() {
        val idTag = UUID.randomUUID().toString()

        val request = AnimalRequestGrpc.newBuilder()
                .setAge(10).setBreed("").setIdTag(idTag)
                .setTemperament("Docil")
                .setAvailableForAdoption(true).setPreviousAdopted(true)
                .build()

        shouldThrow<StatusRuntimeException> {
            grpcClient.create(request)
        }.run {
            message.shouldContain("INVALID_ARGUMENT: ")
        }

    }

}


@Factory
class GrpcClientFactory {

    @Singleton
    fun grpc(@GrpcChannel(GrpcServerChannel.NAME) channel: ManagedChannel) = NewAnimalServiceGrpc.newBlockingStub(channel)

}

