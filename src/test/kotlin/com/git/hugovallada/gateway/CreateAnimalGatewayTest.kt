package com.git.hugovallada.gateway

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.model.Animal
import com.git.hugovallada.repository.AnimalRepository
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*


@ExtendWith(MockKExtension::class)
class CreateAnimalGatewayTest {

    @MockK
    private lateinit var animalRepository: AnimalRepository

    @InjectMockKs
    private lateinit var createAnimalGateway: CreateAnimalGateway

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `should return an animal domain`() {

        val publicId = UUID.randomUUID()
        val idTag = UUID.randomUUID()

        val animalDomain = AnimalDomain(breed = "xx", age = 10, temperament = "x",
                availableForAdoption = true, previousAdopted = true, idTag = idTag
        )

        val animalModel = Animal(id = 1, publicId = publicId, breed = "xx",
                age = 10, temperament = "xx", availableForAdoption = true,
                previousAdopted = true, idTag = idTag
        )

        every { animalRepository.save(any()) } returns animalModel

        val response = createAnimalGateway.execute(animalDomain)

        response.shouldNotBeNull()
        response.shouldBe(AnimalDomain.of(animalModel))

        verify(exactly = 1) { animalRepository.save(any()) }
    }

    @Test
    fun `should throw an internal error`() {
        val errorMessage = "Internal Server Error"

        val idTag = UUID.randomUUID()

        val animalDomain = AnimalDomain(breed = "xx", age = 10, temperament = "x",
                availableForAdoption = true, previousAdopted = true, idTag = idTag
        )

        every { animalRepository.save(any()) } throws RuntimeException(errorMessage)

        shouldThrow<RuntimeException> {
            createAnimalGateway.execute(animalDomain)
        }.run {
            message.shouldBe(errorMessage)
        }

        verify(exactly = 1) { animalRepository.save(any()) }
    }


}