package com.git.hugovallada.usecase

import com.git.hugovallada.domain.AnimalDomain
import com.git.hugovallada.exceptions.AlreadyReportedException
import com.git.hugovallada.gateway.CreateAnimalGateway
import com.git.hugovallada.gateway.ExistsAnimalByIdTagGateway
import com.git.hugovallada.model.Animal
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.equality.shouldBeEqualToComparingFieldsExcept
import io.kotest.matchers.equality.shouldBeEqualToUsingFields
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import io.mockk.called
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
class CreateAnimalUseCaseTest {

    @MockK
    private lateinit var existsAnimalByIdTagGateway: ExistsAnimalByIdTagGateway

    @MockK
    private lateinit var createAnimalGateway: CreateAnimalGateway

    @InjectMockKs
    private lateinit var createAnimalUseCase: CreateAnimalUseCase

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `should return an animal domain`() {
        val idTag = UUID.randomUUID()
        val publicId = UUID.randomUUID()

        val animalDomain = AnimalDomain(breed = "xx", age = 10, temperament = "x",
                availableForAdoption = true, previousAdopted = true, idTag = idTag
        )

        val animalModel = Animal(id = 1, publicId = publicId, breed = "xx",
                age = 10, temperament = "xx", availableForAdoption = true,
                previousAdopted = true, idTag = idTag
        )

        every { existsAnimalByIdTagGateway.execute(any()) } returns false
        every { createAnimalGateway.execute(any()) } returns AnimalDomain.of(animalModel)

        val response = createAnimalUseCase.execute(animalDomain)

        response.idTag.shouldBe(animalDomain.idTag)
        response.id.shouldNotBeNull()
        response.shouldBeTypeOf<AnimalDomain>()
        response.shouldBeEqualToComparingFieldsExcept(animalDomain, ignoreProperty = AnimalDomain::id, ignoreProperties = arrayOf(AnimalDomain::publicId, AnimalDomain::temperament))
        response.shouldBeEqualToUsingFields(animalDomain, properties = arrayOf(AnimalDomain::breed, AnimalDomain::age, AnimalDomain::previousAdopted, AnimalDomain::availableForAdoption, AnimalDomain::idTag))

        verify(exactly = 1) {
            existsAnimalByIdTagGateway.execute(any())
            createAnimalGateway.execute(any())
        }
    }

    @Test
    fun `should throw already reported exception`() {
        val idTag = UUID.randomUUID()
        val publicId = UUID.randomUUID()

        val animalDomain = AnimalDomain(breed = "xx", age = 10, temperament = "x",
                availableForAdoption = true, previousAdopted = true, idTag = idTag
        )

        every { existsAnimalByIdTagGateway.execute(any()) } returns true

        shouldThrow<AlreadyReportedException> {
            createAnimalUseCase.execute(animalDomain)
        }.run {
            message.shouldBe("There's already an animal with the tag: ${animalDomain.idTag}")
        }


        verify(exactly = 1) { existsAnimalByIdTagGateway.execute(any()) }
        verify { createAnimalGateway wasNot called }

    }
}